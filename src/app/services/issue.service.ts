import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, of} from "rxjs";
import {User} from "../model/User";
import {Issue} from "../model/Issue";

@Injectable({
  providedIn: 'root'
})
export class IssueService {

  private url = 'http://localhost:8080/issues'
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient
  ) { }

  getIssues(): Observable<Issue[]> {
    return this.http.get<Issue[]>(this.url);
  }

  getIssueByCode(code: Number): Observable<Issue> {
    return this.http.get<Issue>(this.url+"/"+code);
  }

  update(issue: Issue): Observable<any> {
    let updateUrl = this.url + "/" + issue.code
    return this.http.put(updateUrl, issue, this.httpOptions).pipe(catchError(this.handleError<any>('updateIssue')))
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    console.log(message);
  }
}
