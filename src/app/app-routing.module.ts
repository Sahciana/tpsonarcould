import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UserListComponent} from "./user/user-list/user-list.component";
import {UserDetailsComponent} from "./user/user-details/user-details.component";
import {CommentaireListComponent} from "./commentaire/commentaire-list/commentaire-list.component";
import {IssueDetailsComponent} from "./issue/issue-details/issue-details.component";
import {IssueListComponent} from "./issue/issue-list/issue-list.component";
import {CommentaireDetailsComponent} from "./commentaire/commentaire-details/commentaire-details.component";

const routes: Routes = [
  { path: 'users', component: UserListComponent },
  { path: "users/:id", component: UserDetailsComponent },
  { path: "issues", component: IssueListComponent },
  { path: "issues/:code", component: IssueDetailsComponent },
  { path: "commentaires", component: CommentaireListComponent },
  { path: "commentaires/:id", component: CommentaireDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
