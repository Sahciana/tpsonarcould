import { Component } from '@angular/core';
import {Issue} from "../../model/Issue";
import {ActivatedRoute} from "@angular/router";
import {IssueService} from "../../services/issue.service";

@Component({
  selector: 'app-issue-details',
  templateUrl: './issue-details.component.html',
  styleUrls: ['./issue-details.component.css']
})
export class IssueDetailsComponent {

  issue!:Issue;
  mode: String = "Edit";

  constructor(
    private issueService: IssueService,
    private route: ActivatedRoute )
  {
    const code_issue: Number = Number(this.route.snapshot.paramMap.get("code"))
    this.issueService.getIssueByCode(code_issue).subscribe(issue => this.issue = issue)
  }

  changeMode() {
    if (this.mode==="Edit") {
      this.mode = "Display"
    } else {
      this.mode = "Edit"
    }
  }

  updateIssue()
  {
    console.log("update")
    this.issueService.update(this.issue).subscribe(()=>this.changeMode());
  }

}
