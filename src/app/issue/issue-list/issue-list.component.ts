import { Component } from '@angular/core';
import {Issue} from "../../model/Issue";
import {IssueService} from "../../services/issue.service";

@Component({
  selector: 'app-issue-list',
  templateUrl: './issue-list.component.html',
  styleUrls: ['./issue-list.component.css']
})
export class IssueListComponent {

  issues: Issue[]

  constructor(
    private issueService: IssueService
  ) {
    this.issues = []
    issueService.getIssues()
      .subscribe(issues => this.issues = issues)
  }

}
