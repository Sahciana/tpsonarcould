import {Component, Input} from '@angular/core';
import {Router} from "@angular/router";
import {Issue} from "../../model/Issue";

@Component({
  selector: 'app-issue-block',
  templateUrl: './issue-block.component.html',
  styleUrls: ['./issue-block.component.css']
})
export class IssueBlockComponent {

  @Input() issue!: Issue;
  constructor(private router: Router) {}

  openIssue(code: Number) {
    this.router.navigate(['/issues/'+code])
  }
}
